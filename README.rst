======
Thonny
======

Thonny is a Python IDE meant for learning programming.

End users
---------
See http://thonny.org for more info.


Contributors
------------
Contributions are welcome! See `CONTRIBUTING.rst <https://bitbucket.org/plas/thonny/src/master/CONTRIBUTING.rst>`_ for more info.


UT Student Project Contest 01.06.2018
------------
Poster by Alar Leemet is available `here <https://bitbucket.org/plas/thonny/src/master/omniscient_debugging_thonny_alar_leemet.pdf>`_.
